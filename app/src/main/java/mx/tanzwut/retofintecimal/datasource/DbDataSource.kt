package mx.tanzwut.retofintecimal.datasource

import androidx.room.Database
import androidx.room.RoomDatabase
import mx.tanzwut.retofintecimal.dao.PlaceDao
import mx.tanzwut.retofintecimal.model.Place


@Database(entities = [Place::class], version = 1)
abstract class DbDataSource: RoomDatabase() {

    abstract fun placeDao(): PlaceDao

}