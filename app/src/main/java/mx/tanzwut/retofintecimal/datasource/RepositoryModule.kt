package mx.tanzwut.retofintecimal.datasource

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import mx.tanzwut.retofintecimal.repository.PlaceRepository
import mx.tanzwut.retofintecimal.repository.PlaceRepositoryImp
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun placeRepository(repository: PlaceRepositoryImp): PlaceRepository

}