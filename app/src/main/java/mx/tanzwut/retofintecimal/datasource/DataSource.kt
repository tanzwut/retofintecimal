package mx.tanzwut.retofintecimal.datasource

import mx.tanzwut.retofintecimal.model.Place
import retrofit2.http.GET

interface DataSource {

    @GET("interview")
    suspend fun getPlaces(): List<Place>

}