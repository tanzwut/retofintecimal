package mx.tanzwut.retofintecimal.model

data class PlaceLocation(
    val latitude: Double,
    val longitude: Double
)