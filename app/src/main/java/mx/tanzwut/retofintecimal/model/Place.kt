package mx.tanzwut.retofintecimal.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "place")
data class Place(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "street_name") val streetName: String,
    @ColumnInfo(name = "suburb") val suburb: String,
    @ColumnInfo(name = "visited") var visited: Boolean,
    @Embedded val location: PlaceLocation
)