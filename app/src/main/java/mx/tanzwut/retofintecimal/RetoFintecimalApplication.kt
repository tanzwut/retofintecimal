package mx.tanzwut.retofintecimal

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RetoFintecimalApplication: Application() {
}