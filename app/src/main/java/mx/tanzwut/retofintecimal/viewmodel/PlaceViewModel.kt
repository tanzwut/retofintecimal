package mx.tanzwut.retofintecimal.viewmodel

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import mx.tanzwut.retofintecimal.model.Place
import mx.tanzwut.retofintecimal.repository.PlaceRepository
import mx.tanzwut.retofintecimal.repository.PreferenceRepositoryImp
import javax.inject.Inject

@HiltViewModel
class PlaceViewModel @Inject constructor(
    private val placeRepository: PlaceRepository,
    private val preferenceRepository: PreferenceRepositoryImp
): ViewModel() {

    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData(true)

    private val _isFirstRun: MutableLiveData<Boolean> = MutableLiveData(true)

    val places: LiveData<List<Place>> by lazy {
        placeRepository.getAllPlaces()
    }

    val isLoading: LiveData<Boolean> get() = _isLoading

    val isFirstRun: LiveData<Boolean> get() = _isFirstRun

    fun getFirstRun() {
        viewModelScope.launch(Dispatchers.IO) {
            _isFirstRun.postValue(preferenceRepository.getFirstRun().first())
        }
    }

    fun updateFirstRun(value: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            preferenceRepository.saveFirstRun(value)
        }
    }

    fun checkFirstRun() {
        viewModelScope.launch(Dispatchers.IO) {
            val firstRun = preferenceRepository.getFirstRun().first()

            if (firstRun) {
                placeRepository.getPlaces {
                }

                preferenceRepository.saveFirstRun(false)
            }


            _isLoading.postValue(false)
        }
    }

    fun getPlace(placeId: Int): LiveData<Place> = placeRepository.getPlace(placeId)

    fun setPlaceVisited(place: Place) {
        place.visited = true
        viewModelScope.launch(Dispatchers.IO) {
            placeRepository.updatePlace(place)
        }
    }

    fun bitmapDescriptorFromVector(
        context: Context,
        vectorResId: Int
    ): BitmapDescriptor? {
        val drawable = ContextCompat.getDrawable(context, vectorResId) ?: return null
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        val bm = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )

        val canvas = android.graphics.Canvas(bm)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bm)
    }

    fun startNavigation(context: Context, lat: Double, lon: Double) {
        val uri = "geo:0,0?q=${lat},${lon}"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(uri)
        ContextCompat.startActivity(context, intent, null)
    }
}