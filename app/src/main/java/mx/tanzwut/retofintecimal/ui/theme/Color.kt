package mx.tanzwut.retofintecimal.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Visitada = Color(0xFF3AC2C2)
val StreetName = Color(0xFF333333)
val Suburb = Color(0xFFAAAAAA)
val Pendiente = Color(0xFFAAAAAA)
val PlaceBackground = Color(0xFFFFFFFF)
val ButtonBorder = Color(0xFF2DB0B0)
val NavegarTextButton = Color(0xFF29B9B9)
val AvatarBorder = Color(0xFF3AC2C2)
val SearchPlaceholder = Color(0xFFAAAAAA)
val AllVisitedBackground = Color(0xFFFDFDFD)
val AllVisitedLargeText = Color(0xFF333333)
val AllVisitedSmallText = Color(0xFF7C839D)