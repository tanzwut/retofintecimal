package mx.tanzwut.retofintecimal.repository

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

interface PreferenceRepository {

    suspend fun saveFirstRun(value: Boolean)

    suspend fun getFirstRun(): Flow<Boolean>

}

const val DATASTORE_NAME = "setttings"

val Context.datastore by preferencesDataStore(name = DATASTORE_NAME)

class PreferenceRepositoryImp @Inject constructor(
    @ApplicationContext private val context: Context
): PreferenceRepository {

    companion object {
        val FIRST_RUN = booleanPreferencesKey("first_run")
    }

    override suspend fun saveFirstRun(value: Boolean) {
        context.datastore.edit { settings ->
            settings[PreferenceRepositoryImp.FIRST_RUN] = value
        }
    }

    override suspend fun getFirstRun(): Flow<Boolean> = context.datastore.data.map { preferences ->
            preferences[PreferenceRepositoryImp.FIRST_RUN] ?: true
        }

}