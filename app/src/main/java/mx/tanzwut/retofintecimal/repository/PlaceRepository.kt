package mx.tanzwut.retofintecimal.repository

import androidx.lifecycle.LiveData
import mx.tanzwut.retofintecimal.dao.PlaceDao
import mx.tanzwut.retofintecimal.datasource.DataSource
import mx.tanzwut.retofintecimal.model.Place
import javax.inject.Inject

interface PlaceRepository {

    suspend fun getPlaces(onFinished: ()->Unit)

    fun getAllPlaces(): LiveData<List<Place>>

    fun getPlace(placeId: Int): LiveData<Place>

    fun updatePlace(place: Place)

}

class PlaceRepositoryImp @Inject constructor(
    private val dataSource: DataSource,
    private val placeDao: PlaceDao
): PlaceRepository {

    override suspend fun getPlaces(onFinished: ()->Unit) {
        val places = dataSource.getPlaces()

        placeDao.insertAll(*places.toTypedArray())

        onFinished.invoke()
    }

    override fun getAllPlaces(): LiveData<List<Place>> = placeDao.getPlaces()

    override fun getPlace(placeId: Int): LiveData<Place> = placeDao.getPlace(placeId)

    override fun updatePlace(place: Place) {
        placeDao.updatePlace(place)
    }
}