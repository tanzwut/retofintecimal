package mx.tanzwut.retofintecimal

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.rememberCameraPositionState
import dagger.hilt.android.AndroidEntryPoint
import mx.tanzwut.retofintecimal.model.Place
import mx.tanzwut.retofintecimal.ui.theme.*
import mx.tanzwut.retofintecimal.viewmodel.PlaceViewModel

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            val navController = rememberNavController()

            RetoFintecimalTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color.LightGray
                ) {
                    NavHost(
                        navController = navController,
                        startDestination = "main"
                    ) {
                        composable("main") {
                            MainScene(navController)
                        }
                        composable(
                            route = "detail/{placeId}",
                            arguments = listOf(navArgument("placeId") { type = NavType.IntType })
                        ) { entry ->
                            PlaceDetail(navController, entry.arguments?.getInt("placeId"))
                        }
                    }
                }
            }
        }
    }
}


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MainScene(
    navController: NavHostController,
    viewModel: PlaceViewModel = hiltViewModel()
) {
    val isLoading by viewModel.isLoading.observeAsState(true)
    val places by viewModel.places.observeAsState()

    Scaffold {
        if (isLoading) {
            viewModel.checkFirstRun()
        }
        else {
            if (places != null) {
                Column(
                    modifier = Modifier
                        .padding(it)
                        .fillMaxWidth()
                ) {
                    Row (
                        modifier = Modifier.padding(top = 40.dp)
                            ) {
                        
                        Text(text = buildAnnotatedString {
                            withStyle(style = SpanStyle(fontFamily = FontFamily.SansSerif, fontWeight = FontWeight.Medium)) {
                                append("Hola Ignacio, ")
                            }

                            withStyle(style = SpanStyle(fontSize = 12.sp, fontFamily = FontFamily.SansSerif, fontWeight = FontWeight.Light)) {
                                append("buenos días!")
                            }
                        }, modifier = Modifier.padding(start = 20.dp))

                        Spacer(modifier = Modifier.weight(1f))

                        Image(
                            painter = painterResource(id = R.drawable.avatar),
                            contentDescription = "Profile Pic",
                            modifier = Modifier
                                .padding(bottom = 20.dp, end = 30.dp)
                                .size(26.dp)
                                .clip(CircleShape)
                                .border(1.dp, AvatarBorder, CircleShape)
                        )
                    }
                    var text by remember {
                        mutableStateOf(TextFieldValue(""))
                    }
                    val keyboardController = LocalSoftwareKeyboardController.current
                    val focusManager = LocalFocusManager.current

                    TextField(value = text,
                        onValueChange = { newText ->
                            text = newText
                        },
                        placeholder = { Text(text = "Busca una visita", fontSize = 12.sp, fontFamily = FontFamily.SansSerif, color = SearchPlaceholder) },
                        textStyle = TextStyle(fontSize = 12.sp, fontFamily = FontFamily.SansSerif),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                        keyboardActions = KeyboardActions(onDone = {
                            focusManager.clearFocus()
                            keyboardController?.hide()
                        }),
                        colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White, focusedIndicatorColor = Color.Transparent, unfocusedIndicatorColor = Color.Transparent),
                        shape = RoundedCornerShape(8.dp),
                        modifier = Modifier
                            .padding(horizontal = 20.dp)
                            .fillMaxWidth()
                            .height(48.dp)
                            .shadow(3.dp, shape = RoundedCornerShape(8.dp))
                    )

                    if (places!!.none { place -> !place.visited })
                    {
                        Card(
                            backgroundColor = AllVisitedBackground,
                            modifier = Modifier.padding(start = 20.dp, top = 60.dp, end = 20.dp).fillMaxWidth()
                        ) {
                            Column(
                                modifier = Modifier.padding(vertical = 16.dp),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Image(painter = painterResource(id = R.drawable.ic_visited_marker), contentDescription = "Visited Marker", modifier = Modifier.padding(bottom = 10.dp).size(28.dp))
                                
                                Text(text = "¡Wow! Estás al día con tus visitas", fontSize = 14.sp, fontFamily = FontFamily.SansSerif, fontWeight = FontWeight.Medium, color = AllVisitedLargeText)

                                Text(text = "Te avisaremos cuando haya más", fontSize = 12.sp, fontFamily = FontFamily.SansSerif, fontWeight = FontWeight.Normal, color = AllVisitedSmallText)
                            }
                        }
                    }
                    else {
                        Text(
                            text = "Tienes ${places!!.filter { place -> !place.visited }.size} visitas por hacer",
                            modifier = Modifier
                                .padding(
                                    start = 20.dp,
                                    top = 30.dp,
                                    end = 20.dp,
                                    bottom = 20.dp
                                )
                                .fillMaxWidth(),
                            fontSize = 16.sp,
                            fontFamily = FontFamily.SansSerif,
                            fontWeight = FontWeight.Medium
                        )

                        LazyColumn(modifier = Modifier.padding(it)) {
                            items(places!!.filter { place ->
                                if (text.text.isNotBlank()) place.streetName.contains(
                                    text.text,
                                    true
                                ) || place.suburb.contains(text.text, true) else true
                            }) { place ->
                                PlaceListRow(place = place) { selected ->
                                    navController.navigate("detail/${selected.id}")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PlaceListRow(
    place: Place,
    onPlaceSelected: (Place)->Unit
) {
    Card(
        modifier = Modifier
            .padding(horizontal = 20.dp, vertical = 6.dp)
            .height(70.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(10.dp),
        onClick = {
                  onPlaceSelected.invoke(place)
        },
        backgroundColor = PlaceBackground,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(14.dp)
        ) {

            Canvas(modifier = Modifier
                .padding(start = 14.dp)
                .size(8.dp), onDraw = {
                drawCircle(color = if (place.visited) Visitada else Pendiente)
            })

            Column(
                modifier = Modifier.weight(1f)
            ) {
                Text(
                    text = if (place.visited) "Visitada" else "Pendiente",
                    fontFamily = FontFamily.SansSerif,
                    fontWeight = FontWeight.Medium,
                    fontSize = 11.sp,
                    color = if (place.visited) Visitada else Pendiente
                )
                Text(
                    text = place.streetName,
                    fontFamily = FontFamily.SansSerif,
                    fontWeight = FontWeight.Medium,
                    fontSize = 13.sp,
                    color = StreetName
                )
                Text(
                    text = place.suburb,
                    fontFamily = FontFamily.SansSerif,
                    fontWeight = FontWeight.Normal,
                    fontSize = 12.sp,
                    color = Suburb
                )
            }

            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = "Place arrow",
                modifier = Modifier.padding(end = 20.dp)
            )
        }
    }
}

@Composable
fun PlaceDetail(
    navController: NavHostController,
    placeId: Int?,
    viewModel: PlaceViewModel = hiltViewModel()
) {
    val place = placeId?.let { id ->
        viewModel.getPlace(id)
    }?.observeAsState()

    Scaffold {
        Box(modifier = Modifier.padding(it)) {
            if (place?.value != null) {
                val location =
                    LatLng(place.value!!.location.latitude, place.value!!.location.longitude)
                val cameraPositionState = rememberCameraPositionState {
                    position = CameraPosition.fromLatLngZoom(location, 15f)
                }

                GoogleMap(
                    modifier = Modifier.fillMaxSize(),
                    cameraPositionState = cameraPositionState,
                    uiSettings = MapUiSettings(
                        compassEnabled = false,
                        indoorLevelPickerEnabled = false,
                        mapToolbarEnabled = false,
                        myLocationButtonEnabled = false,
                        rotationGesturesEnabled = false,
                        scrollGesturesEnabled = false,
                        scrollGesturesEnabledDuringRotateOrZoom = false,
                        tiltGesturesEnabled = false,
                        zoomControlsEnabled = false,
                        zoomGesturesEnabled = false
                    )
                ) {
                    val icon = viewModel.bitmapDescriptorFromVector(
                        LocalContext.current,
                        if (place.value!!.visited) R.drawable.ic_visited_marker else R.drawable.ic_marker
                    )

                    Marker(
                        position = location,
                        icon = icon
                    )
                }

                Column(
                    modifier = Modifier.padding(it)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                    ) {
                        IconButton(onClick = {
                            navController.popBackStack()
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_nav_back),
                                contentDescription = "Nav back button"
                            )
                        }

                        Spacer(modifier = Modifier.weight(1f))
                    }

                    Spacer(modifier = Modifier.weight(1f))

                    Card(
                        shape = RoundedCornerShape(10.dp),
                        backgroundColor = PlaceBackground,
                        modifier = Modifier
                            .padding(20.dp)
                            .fillMaxWidth()
                            .height(if (place.value!!.visited) 70.dp else 130.dp)
                    ) {
                        Column(
                            verticalArrangement = Arrangement.SpaceBetween
                        ) {

                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.spacedBy(15.dp),
                                modifier = Modifier.padding(top = 10.dp)
                            ) {

                                Canvas(
                                    modifier = Modifier
                                        .padding(start = 14.dp)
                                        .size(8.dp),
                                    onDraw = {
                                        drawCircle(color = if (place.value!!.visited) Visitada else Pendiente)
                                    })

                                Column(
                                    modifier = Modifier.weight(1f)
                                ) {
                                    Text(
                                        text = if (place.value!!.visited) "Visitada" else "Pendiente",
                                        fontFamily = FontFamily.SansSerif,
                                        fontWeight = FontWeight.Medium,
                                        fontSize = 11.sp,
                                        color = if (place.value!!.visited) Visitada else Pendiente
                                    )
                                    Text(
                                        text = place.value!!.streetName,
                                        fontFamily = FontFamily.SansSerif,
                                        fontWeight = FontWeight.Medium,
                                        fontSize = 13.sp,
                                        color = StreetName
                                    )
                                    Text(
                                        text = place.value!!.suburb,
                                        fontFamily = FontFamily.SansSerif,
                                        fontWeight = FontWeight.Normal,
                                        fontSize = 12.sp,
                                        color = Suburb
                                    )
                                }
                            }

                            if (!place.value!!.visited) {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically,
                                ) {
                                    val context = LocalContext.current

                                    OutlinedButton(
                                        modifier = Modifier
                                            .padding(15.dp)
                                            .height(40.dp)
                                            .width(115.dp),
                                        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
                                        onClick = {
                                            viewModel.startNavigation(
                                                context,
                                                place.value!!.location.latitude,
                                                place.value!!.location.longitude
                                            )
                                        },
                                        border = BorderStroke(1.dp, ButtonBorder),
                                        shape = RoundedCornerShape(5.dp)
                                    ) {
                                        Text(
                                            text = "Navegar",
                                            fontSize = 14.sp,
                                            fontFamily = FontFamily.SansSerif,
                                            fontWeight = FontWeight.Medium,
                                            textAlign = TextAlign.Center,
                                            color = NavegarTextButton
                                        )
                                    }

                                    Button(
                                        modifier = Modifier
                                            .padding(15.dp)
                                            .height(40.dp)
                                            .width(175.dp),
                                        colors = ButtonDefaults.buttonColors(backgroundColor = Visitada),
                                        onClick = {
                                            navController.popBackStack()
                                            viewModel.setPlaceVisited(place.value!!)
                                        },
                                        border = BorderStroke(1.dp, ButtonBorder),
                                        shape = RoundedCornerShape(5.dp)
                                    ) {
                                        Text(
                                            text = "Realizar visita",
                                            fontSize = 14.sp,
                                            fontFamily = FontFamily.SansSerif,
                                            fontWeight = FontWeight.Medium,
                                            textAlign = TextAlign.Center,
                                            color = Color.White
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    RetoFintecimalTheme {
        val navController = rememberNavController()
        MainScene(navController)
    }
}