package mx.tanzwut.retofintecimal.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import mx.tanzwut.retofintecimal.dao.PlaceDao
import mx.tanzwut.retofintecimal.datasource.DataSource
import mx.tanzwut.retofintecimal.datasource.DbDataSource
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {

    @Singleton
    @Provides
    @Named("BaseURL")
    fun provideBaseUrl() = "https://fintecimal-test.herokuapp.com/api/"

    @Singleton
    @Provides
    fun provideRetrofit(@Named("BaseURL") baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
    }

    @Singleton
    @Provides
    fun dataSource(retrofit: Retrofit): DataSource = retrofit.create(DataSource::class.java)

    @Singleton
    @Provides
    fun dbDataSource(@ApplicationContext context: Context): DbDataSource {
        return Room.databaseBuilder(context, DbDataSource::class.java, "place_database")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun placeDao(db: DbDataSource): PlaceDao = db.placeDao()

}