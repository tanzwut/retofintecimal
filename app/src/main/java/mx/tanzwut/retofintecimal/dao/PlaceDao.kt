package mx.tanzwut.retofintecimal.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mx.tanzwut.retofintecimal.model.Place

@Dao
interface PlaceDao {

    @Query("SELECT * FROM place ORDER BY visited DESC")
    fun getPlaces(): LiveData<List<Place>>

    @Query("SELECT * FROM place WHERE id = :placeId")
    fun getPlace(placeId: Int): LiveData<Place>

    @Insert(entity = Place::class, onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg places: Place)

    @Update(entity = Place::class, onConflict = OnConflictStrategy.REPLACE)
    fun updatePlace(place: Place)

}